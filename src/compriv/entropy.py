"""
Entropy indicators for tabular objects.
"""

from functools import singledispatch
from typing import Optional

import numpy as np
import pandas as pd
import polars as pl


@singledispatch
def entropy(d) -> Optional[float | pl.DataFrame | pd.DataFrame]:
    """Entropy of a Series or DataFrame object. If the object
    is a DataFrame, entropy per column is computed.

    Args:
        d: polars or pandas DataFrame or Series object

    Returns:
        Optional[float | pl.DataFrame | pd.DataFrame]: uniqueness value or DataFrame
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@entropy.register
def _(d: pl.Series) -> Optional[float]:
    if len(d) == 0:
        return None
    if d.dtype == pl.Boolean:
        counts = d.value_counts().get_column("counts")
    else:
        counts = d.unique_counts()
    return counts.entropy(normalize=True)


@entropy.register
def _(d: pl.DataFrame) -> Optional[pl.DataFrame]:
    if d.height == 0 or d.width == 0:
        return None
    return d.select(pl.all().map(entropy)).melt().rename({"value": "entropy"})


@entropy.register
def _(d: pd.Series) -> Optional[float]:
    if len(d) == 0:
        return None
    counts = d.value_counts(normalize=True, dropna=False)
    return -((counts * np.log(counts)).sum())


@entropy.register
def _(d: pd.DataFrame) -> Optional[float]:
    if 0 in d.shape:
        return None
    entropies = d.aggregate(entropy, axis=0)
    entropies = pd.DataFrame(entropies).reset_index()
    entropies.columns = ["variable", "entropy"]
    return entropies


if __name__ == "__main__":
    print(entropy(pd.DataFrame(dict(x=[1, 1], y=["a", "b"]))))
