"""
Basic indicators for tabular objects.
"""

from functools import singledispatch
from typing import Optional

import pandas as pd
import polars as pl


@singledispatch
def n_rows(d) -> Optional[int]:
    """Number of rows of a DataFrame object

    Args:
        d: polars or pandas DataFrame object

    Returns:
        Optional[int]: number of rows or None
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@n_rows.register
def _(d: pl.DataFrame) -> int:
    return d.height


@n_rows.register
def _(d: pd.DataFrame) -> int:
    return d.shape[0]


@singledispatch
def n_cols(d) -> Optional[int]:
    """Number of columns of a DataFrame object

    Args:
        d: polars or pandas DataFrame object

    Returns:
        Optional[int]: number of columns or None
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@n_cols.register
def _(d: pl.DataFrame) -> int:
    return d.width


@n_cols.register
def _(d: pd.DataFrame) -> int:
    return d.shape[1]
