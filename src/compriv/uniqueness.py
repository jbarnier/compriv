"""
Uniqueness indicators for tabular objects.
"""

from functools import singledispatch
from typing import Optional

import pandas as pd
import polars as pl


@singledispatch
def uniqueness(d) -> Optional[float]:
    """Uniqueness of rows of a DataFrame object. Computed as the
    proportion of unique rows: if 0, all rows are the same, if 1,
    all rows are different.

    Args:
        d: polars or pandas DataFrame or Series object

    Returns:
        Optional[float]: uniqueness value or None
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@uniqueness.register
def _(d: pl.DataFrame) -> Optional[float]:
    if d.height == 0 or d.width == 0:
        return None
    res = d.is_unique().sum() / d.height
    return res


@uniqueness.register
def _(d: pl.Series) -> Optional[float]:
    if len(d) == 0:
        return None
    res = d.is_unique().sum() / len(d)
    return res


@uniqueness.register
def _(d: pd.DataFrame) -> Optional[float]:
    if 0 in d.shape:
        return None
    res = (~d.duplicated(keep=False)).sum() / d.shape[0]
    return res


@uniqueness.register
def _(d: pd.Series) -> Optional[float]:
    if len(d) == 0:
        return None
    res = (~d.duplicated(keep=False)).sum() / len(d)
    return res
