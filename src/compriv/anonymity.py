"""
Anonymity indicators for tabular objects.
"""

from functools import singledispatch
from typing import Optional, Sequence
import matplotlib.pyplot as plt
from matplotlib.axes._axes import Axes
from matplotlib import ticker

import pandas as pd
import polars as pl


@singledispatch
def anonymity_set_size(
    d: pl.DataFrame | pd.DataFrame, cols: Optional[Sequence[str]] = None
) -> Optional[pl.Series | pd.Series]:
    """Anonymity set size of each row of a DataFrame object.

    Args:
        d: polars or pandas DataFrame object.
        cols (Sequence[str], optional): list of columns to compute on.
            If None, compute anonymity set size on all columns. Defaults
            to None.

    Returns:
        Optional[pl.Series | pd.Series]: anonymity set size values.
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@anonymity_set_size.register
def _(d: pl.DataFrame, cols: Optional[Sequence[str]] = None) -> Optional[pl.Series]:
    if d.height == 0 or d.width == 0:
        return None
    if cols is None:
        cols = d.columns
    res = d.with_columns(
        [pl.col(d.columns[0]).len().over(cols).alias("ano_set_size")]
    ).get_column("ano_set_size")
    return res


@anonymity_set_size.register
def _(d: pd.DataFrame, cols: Optional[Sequence[str]] = None) -> Optional[pd.Series]:
    if 0 in d.shape:
        return None
    if cols is None:
        cols = list(d.columns)
    counts = d.groupby(cols, dropna=False, as_index=False).value_counts()
    res = d.merge(counts, how="left", on=cols)
    res = res.rename(columns={"count": "ano_set_size"})
    return res.loc[:, "ano_set_size"]


def anonymity_set_size_cdf(
    ano_data: pd.Series | pl.Series, ax: Axes = None, title: str = ""
) -> Axes:
    """Plot the anonymity set size cumulative distribution function.

    Args:
        ano_data (pd.Series | pl.Series): anonymity set size values, such as computed
            by anonymity_set_size().
        ax (Axes, optional): optional matplotlin axis. If None, a new figure is created.
            Defaults to None.
        title (str, optional): plot title. Defaults to "".

    Returns:
        Axes: a matplotlib axis.
    """
    d = (
        pd.Series(ano_data)
        .value_counts(normalize=True, sort=False)
        .sort_index()
        .cumsum()
    )
    x = d.index
    y = d * 100
    if ax is None:
        fig, ax = plt.subplots()
    ax.stairs(
        [0, *y],
        edges=[0, *x, x.max() * 1.1],  # type: ignore
        fill=True,
        facecolor="#FFF0F0",
        edgecolor="#800",
        linewidth=1,
    )
    ax.set_facecolor("#EAEAF2")
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)
    ax.spines["bottom"].set_visible(False)
    ax.spines["left"].set_visible(False)
    ax.grid(visible=True, axis="y", color="white")
    ax.set_axisbelow(True)
    ax.set_xlim(left=0.8, right=x.max() * 1.08)  # type: ignore
    ax.set_xlabel("Anonymity set size", fontsize=7)
    ax.xaxis.set_tick_params(labelsize=7)
    ax.xaxis.set_major_locator(ticker.MaxNLocator(nbins="auto", integer=True))
    ax.set_ylim(bottom=-3, top=103)
    ax.set_ylabel("Percentage of observations", fontsize=7)
    ax.yaxis.set_tick_params(labelsize=7)
    ax.yaxis.set_major_formatter(ticker.PercentFormatter())
    ax.set_title(f"{title}", fontsize=8)
    return ax


@singledispatch
def multirelational_anonymity(
    d: pl.DataFrame | pd.DataFrame,
    id_col: str,
    cols: Sequence[str] | str,
) -> Optional[pd.DataFrame | pd.DataFrame]:
    """Multirelational k-anonymity of a DataFrame object.

    Args:
        d: polars or pandas DataFrame object.

        id_col (str): column identifying rows belonging to the same individual
            observation.
        cols (Sequence[str]): list of columns containing records for each
            individual observation.

    Returns:
        Optional[pl.DataFrame | pd.DataFrame]: multirelational k-anonymity values.
    """
    if d is None:
        return None
    raise TypeError("Wrong argument type.")


@multirelational_anonymity.register
def _(
    d: pl.DataFrame,
    id_col: str,
    cols: Sequence[str] | str,
) -> Optional[pl.DataFrame]:
    if d.height == 0 or d.width == 0:
        return None

    res = (
        d.with_columns(pl.col(cols).cast(pl.Utf8))
        .groupby(pl.col(id_col), maintain_order=True)
        .agg(cols)
        # Conversion to string is not ideal, but polars can't hash list
        # values for now
        .with_columns([pl.col(cols).list.join("|compriv|")])
        .with_columns([pl.col(id_col).len().over(cols).alias("multi_k")])
        .select([id_col, "multi_k"])
    )

    return res


@multirelational_anonymity.register
def _(
    d: pd.DataFrame,
    id_col: str,
    cols: Sequence[str] | str,
) -> Optional[pd.DataFrame]:
    if 0 in d.shape:
        return None
    if isinstance(cols, (list, tuple)):
        columns = cols.copy()
        columns.append(id_col)
    else:
        columns = [id_col, cols]
    tmp = d[columns]
    # Conversion to string is not ideal, but pandas can't hash list
    # values for now
    tmp[cols] = tmp[cols].astype("string")
    tmp[cols] = tmp[cols].fillna("|compriv_na|")
    tmp = (
        tmp.groupby(id_col).agg(lambda x: ("|compriv|").join(x.to_list())).reset_index()
    )
    counts = tmp.value_counts(subset=cols)
    counts.name = "multi_k"
    counts = counts.reset_index()

    res = tmp.merge(counts, on=cols, how="left")

    return res[[id_col, "multi_k"]]
