"""
Tests for entropy.py
"""

import pytest

from compriv import entropy
import pandas as pd
import polars as pl


s0 = ["a", "a", "a"]
s1 = [1, 1, 2, 2, 3]
s2 = [True, True, False]
s3 = [1, 1, 2, None]
d = dict(x=[1, 1, 1, 2], y=["foo", "bar", "baz", "baz"])


class TestEntropy:
    def test_entropy_polars_series(self):
        assert entropy(pl.Series(s0)) == 0.0
        assert entropy(pl.Series(s1)) == pl.Series([0.4, 0.4, 0.2]).entropy()
        assert entropy(pl.Series(s2)) == pl.Series([2 / 3, 1 / 3]).entropy()
        assert entropy(pl.Series(s3)) == pl.Series([1 / 2, 1 / 4, 1 / 4]).entropy()
        assert entropy(pl.Series()) is None

    def test_entropy_polars_df(self):
        res = pl.DataFrame(
            {
                "variable": ["x", "y"],
                "entropy": [
                    pl.Series([3 / 4, 1 / 4]).entropy(),
                    pl.Series([1 / 4, 1 / 4, 1 / 2]).entropy(),
                ],
            }
        )
        assert entropy(pl.DataFrame(d)).frame_equal(res)
        assert entropy(pl.DataFrame()) is None

    def test_entropy_pandas_series(self):
        assert entropy(pd.Series(s0)) == 0.0
        assert entropy(pd.Series(s1)) == pl.Series([0.4, 0.4, 0.2]).entropy()
        assert entropy(pd.Series(s2)) == pl.Series([2 / 3, 1 / 3]).entropy()
        assert entropy(pd.Series(s3)) == pl.Series([1 / 2, 1 / 4, 1 / 4]).entropy()
        assert entropy(pd.Series(dtype=float)) is None

    def test_entropy_pandas_df(self):
        res = pd.DataFrame(
            {
                "variable": ["x", "y"],
                "entropy": [
                    pl.Series([3 / 4, 1 / 4]).entropy(),
                    pl.Series([1 / 4, 1 / 4, 1 / 2]).entropy(),
                ],
            }
        )
        assert entropy(pd.DataFrame(d)).equals(res)
        assert entropy(pd.DataFrame()) is None

    def test_entropy_other_types(self):
        with pytest.raises(TypeError):
            entropy(3)
        with pytest.raises(TypeError):
            entropy(["a", "b"])
        assert entropy(None) is None
