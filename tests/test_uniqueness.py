"""
Tests for uniqueness.py
"""

import pytest

from compriv import uniqueness
import pandas as pd
import polars as pl


dict1 = dict(x=[1, 2, 3], y=["aa", "b", "c"])
dict0 = dict(x=[1, 1, 1], y=["aa", "aa", "aa"])
dict_m = dict(x=[1, 1, 2, 2, 2, 2], y=["a", "a", "b", "b", "a", "c"])


class TestUniqueness:
    def test_uniqueness_polars_df(self):
        assert uniqueness(pl.DataFrame(dict1)) == 1.0
        assert uniqueness(pl.DataFrame(dict0)) == 0.0
        assert uniqueness(pl.DataFrame(dict_m)) == 1 / 3
        assert uniqueness(pl.DataFrame()) is None

    def test_uniqueness_polars_series(self):
        assert uniqueness(pl.DataFrame(dict1).get_column("x")) == 1.0
        assert uniqueness(pl.DataFrame(dict0).get_column("x")) == 0.0
        assert uniqueness(pl.DataFrame(dict_m).get_column("y")) == 1 / 6
        assert uniqueness(pl.DataFrame()) is None

    def test_uniqueness_pandas_df(self):
        assert uniqueness(pd.DataFrame(dict1)) == 1.0
        assert uniqueness(pd.DataFrame(dict0)) == 0.0
        assert uniqueness(pd.DataFrame(dict_m)) == 1 / 3
        assert uniqueness(pd.DataFrame()) is None

    def test_uniqueness_pandas_series(self):
        assert uniqueness(pd.DataFrame(dict1)["x"]) == 1.0
        assert uniqueness(pd.DataFrame(dict0)["x"]) == 0.0
        assert uniqueness(pd.DataFrame(dict_m)["y"]) == 1 / 6
        assert uniqueness(pd.DataFrame()) is None

    def test_uniqueness_other_types(self):
        with pytest.raises(TypeError):
            uniqueness(3)
        with pytest.raises(TypeError):
            uniqueness(["a", "b"])
        assert uniqueness(None) is None
