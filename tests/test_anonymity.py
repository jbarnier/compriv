"""
Tests for anonymity.py
"""

import pytest

from compriv import anonymity_set_size, multirelational_anonymity
import pandas as pd
import polars as pl


d1 = dict(x=[1, 1, 1, 2], y=["foo", "bar", "bar", "baz"])
d1_res = [1, 2, 2, 1]
d1_res_x = [3, 3, 3, 1]
d2 = dict(x=[1.1, 1.2, 1.2, 2.0], y=["foo", "bar", "bar", "baz"])
d2_res = [1, 2, 2, 1]
d_none = dict(x=[1, None, 1, 2], y=["foo", "bar", "bar", None])
d_none_res = [1, 1, 1, 1]

d_multi1 = dict(
    id=[100, 100, 200, 200, 300, 300, 400, 400, 500, 600, 700],
    v1=["a", "b", "a", "b", "a", "c", "a", "b", None, None, "a"],
    v2=[111, 222, 111, 222, 111, 222, 111, 333, 444, 444, 111],
    foo=list(range(11)),
)
d_multi1_v1_v2_res = dict(
    id=[100, 200, 300, 400, 500, 600, 700], multi_k=[2, 2, 1, 1, 2, 2, 1]
)
d_multi1_v1_res = dict(
    id=[100, 200, 300, 400, 500, 600, 700], multi_k=[3, 3, 1, 3, 2, 2, 1]
)
d_multi1_v2_res = dict(
    id=[100, 200, 300, 400, 500, 600, 700], multi_k=[3, 3, 3, 1, 2, 2, 1]
)


class TestAnonymitySetSize:
    def test_set_size_polars(self):
        assert anonymity_set_size(pl.DataFrame(d1)).series_equal(
            pl.Series(name="ano_set_size", values=d1_res)
        )
        assert anonymity_set_size(pl.DataFrame(d1), cols=["x"]).series_equal(
            pl.Series(name="ano_set_size", values=d1_res_x)
        )
        assert anonymity_set_size(pl.DataFrame(d2)).series_equal(
            pl.Series(name="ano_set_size", values=d2_res)
        )
        assert anonymity_set_size(pl.DataFrame(d_none)).series_equal(
            pl.Series(name="ano_set_size", values=d_none_res)
        )
        assert anonymity_set_size(pl.DataFrame()) is None

    def test_set_size_pandas(self):
        assert anonymity_set_size(pd.DataFrame(d1)).equals(
            pd.Series(name="ano_set_size", data=d1_res)
        )
        assert anonymity_set_size(pd.DataFrame(d2)).equals(
            pd.Series(name="ano_set_size", data=d2_res)
        )
        assert anonymity_set_size(pd.DataFrame(d_none)).equals(
            pd.Series(name="ano_set_size", data=d_none_res)
        )
        assert anonymity_set_size(pd.DataFrame()) is None

    def test_set_size_others(self):
        with pytest.raises(TypeError):
            anonymity_set_size("foo")
        with pytest.raises(TypeError):
            anonymity_set_size(list(1, 2, 3))
        assert anonymity_set_size(None) is None


class TestMultirelationalAnonymity:
    def test_multirelational_anonymity_polars(self):
        assert multirelational_anonymity(
            pl.DataFrame(d_multi1), id_col="id", cols=["v1", "v2"]
        ).frame_equal(pl.DataFrame(d_multi1_v1_v2_res))
        assert multirelational_anonymity(
            pl.DataFrame(d_multi1), id_col="id", cols="v1"
        ).frame_equal(pl.DataFrame(d_multi1_v1_res))
        assert multirelational_anonymity(
            pl.DataFrame(d_multi1), id_col="id", cols="v2"
        ).frame_equal(pl.DataFrame(d_multi1_v2_res))

    def test_multirelational_anonymity_pandas(self):
        assert multirelational_anonymity(
            pd.DataFrame(d_multi1), id_col="id", cols=["v1", "v2"]
        ).equals(pd.DataFrame(d_multi1_v1_v2_res))
        assert multirelational_anonymity(
            pd.DataFrame(d_multi1), id_col="id", cols="v1"
        ).equals(pd.DataFrame(d_multi1_v1_res))
        assert multirelational_anonymity(
            pd.DataFrame(d_multi1), id_col="id", cols="v2"
        ).equals(pd.DataFrame(d_multi1_v2_res))

    def test_multirelational_anonymity_others(self):
        with pytest.raises(TypeError):
            multirelational_anonymity("foo")
        with pytest.raises(TypeError):
            multirelational_anonymity(list(1, 2, 3))
        assert multirelational_anonymity(None, id_col="foo", cols="bar") is None
        assert (
            multirelational_anonymity(pl.DataFrame(), id_col="foo", cols="bar") is None
        )
        assert (
            multirelational_anonymity(pd.DataFrame(), id_col="foo", cols="bar") is None
        )
