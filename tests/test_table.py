"""
Tests for py
"""

import pytest

from compriv import n_rows, n_cols
import pandas as pd
import polars as pl


dict1 = dict(x=[1, 2, 3], y=["aa", "b", "c"])


@pytest.fixture
def d_pl1():
    return pl.DataFrame(dict1)


@pytest.fixture
def d_pd1():
    return pd.DataFrame(dict1)


class TestNrows:
    def test_n_rows_polars(self, d_pl1):
        assert n_rows(d_pl1) == 3

    def test_n_rows_pandas(self, d_pd1):
        assert n_rows(d_pd1) == 3

    def test_n_rows_polars_empty(self):
        assert n_rows(pl.DataFrame()) == 0

    def test_n_rows_pandas_empty(self):
        assert n_rows(pd.DataFrame()) == 0

    def test_n_rows_other(self):
        with pytest.raises(TypeError):
            n_rows("foo")
        assert n_rows(None) is None


class TestCols:
    def test_n_cols_polars(self, d_pl1):
        assert n_cols(d_pl1) == 2

    def test_n_cols_pandas(self, d_pd1):
        assert n_cols(d_pd1) == 2

    def test_n_rcols_polars_empty(self):
        assert n_cols(pl.DataFrame()) == 0

    def test_n_rcols_pandas_empty(self):
        assert n_cols(pd.DataFrame()) == 0

    def test_n_cols_other(self):
        with pytest.raises(TypeError):
            n_cols("foo")
        assert n_cols(None) is None
