import os
import tempfile

import nox

os.environ.update({"PDM_USE_VENV": "1", "PDM_IGNORE_SAVED_PYTHON": "1"})


@nox.session(python=["3.10", "3.11"])
def tests(session):
    session.run("pdm", "install", "-dG", "test", external=True)
    session.run("pytest", "tests")


lint_locations = "src", "tests", "noxfile.py"


@nox.session
def lint(session):
    session.run("pdm", "install", "-dG", "lint", external=True)
    session.run("ruff", "check", *lint_locations)
    session.run("black", *lint_locations)


@nox.session
def safety(session):
    session.run("pdm", "install", "-dG", "security", external=True)
    with tempfile.NamedTemporaryFile() as requirements:
        session.run(
            "pdm",
            "export",
            "--dev",
            "--format=requirements",
            "--without-hashes",
            f"--output={requirements.name}",
            external=True,
        )
        session.install("safety")
        session.run("safety", "check", f"--file={requirements.name}", "--full-report")
