# compriv

`compriv` is a small Python package which performs some basic privacy metrics computations. Source data can be either in a [pandas](https://pandas.pydata.org/) or a [polars](https://www.pola.rs) Series or DataFrame.

Computations are generally quite faster with polars.


## Installation

Project dependencies are managed by [poetry](https://python-poetry.org). Just run `poetry install` after cloning the repository.


## Usage

The package provides the following functions:

- `n_rows`, `n_cols`: dimensions of a data frame
- `uniqueness`: rows uniqueness of a Series or a DataFrame (proportion of unique rows)
- `entropy`: entropy of a Series or a DataFrame
- `anonymity_set_size`: compute the k-anonymity of each row of a Series or a DataFrame
- `anonymity_set_size_cdf`: plot the cumulative distribution function of an anonymity set size result
- `multirelational_anonymity`: computes the multirelational k-anonymity of a DataFrame
